'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * addColumn "roomname" to table "user_game_histories"
 * addColumn "player1" to table "user_game_histories"
 * addColumn "player2" to table "user_game_histories"
 * changeColumn "win" on table "user_game_histories"
 * changeColumn "win" on table "user_game_histories"
 * changeColumn "lose" on table "user_game_histories"
 * changeColumn "lose" on table "user_game_histories"
 *
 **/

var info = {
    "revision": 2,
    "name": "noname",
    "created": "2022-06-12T15:25:30.818Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "addColumn",
        params: [
            "user_game_histories",
            "roomname",
            {
                "type": Sequelize.STRING,
                "field": "roomname",
                "allowNull": false
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "user_game_histories",
            "player1",
            {
                "type": Sequelize.STRING,
                "field": "player1",
                "allowNull": true
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "user_game_histories",
            "player2",
            {
                "type": Sequelize.STRING,
                "field": "player2",
                "allowNull": true
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "user_game_histories",
            "win",
            {
                "type": Sequelize.STRING,
                "field": "win",
                "allowNull": true
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "user_game_histories",
            "win",
            {
                "type": Sequelize.STRING,
                "field": "win",
                "allowNull": true
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "user_game_histories",
            "lose",
            {
                "type": Sequelize.STRING,
                "field": "lose",
                "allowNull": true
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "user_game_histories",
            "lose",
            {
                "type": Sequelize.STRING,
                "field": "lose",
                "allowNull": true
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
