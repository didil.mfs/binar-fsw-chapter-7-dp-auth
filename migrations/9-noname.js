'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * addColumn "p2score" to table "user_game_histories"
 * addColumn "p1score" to table "user_game_histories"
 *
 **/

var info = {
    "revision": 9,
    "name": "noname",
    "created": "2022-06-13T09:45:37.085Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "addColumn",
        params: [
            "user_game_histories",
            "p2score",
            {
                "type": Sequelize.INTEGER,
                "field": "p2score",
                "allowNull": true
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "user_game_histories",
            "p1score",
            {
                "type": Sequelize.INTEGER,
                "field": "p1score",
                "allowNull": true
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
