'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * addColumn "role" to table "user_games"
 *
 **/

var info = {
    "revision": 10,
    "name": "noname",
    "created": "2022-06-16T13:07:39.069Z",
    "comment": ""
};

var migrationCommands = [{
    fn: "addColumn",
    params: [
        "user_games",
        "role",
        {
            "type": Sequelize.STRING,
            "field": "role",
            "defaultValue": "user",
            "allowNull": false
        }
    ]
}];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
