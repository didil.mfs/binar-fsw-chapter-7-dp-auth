'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * removeColumn "player2" from table "user_game_histories"
 * removeColumn "player1" from table "user_game_histories"
 * addColumn "p2choice" to table "user_game_histories"
 * addColumn "p1choice" to table "user_game_histories"
 * addColumn "p2username" to table "user_game_histories"
 * addColumn "p1username" to table "user_game_histories"
 *
 **/

var info = {
    "revision": 4,
    "name": "noname",
    "created": "2022-06-12T16:20:41.475Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "removeColumn",
        params: ["user_game_histories", "player2"]
    },
    {
        fn: "removeColumn",
        params: ["user_game_histories", "player1"]
    },
    {
        fn: "addColumn",
        params: [
            "user_game_histories",
            "p2choice",
            {
                "type": Sequelize.ARRAY(Sequelize.STRING),
                "field": "p2choice",
                "allowNull": true
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "user_game_histories",
            "p1choice",
            {
                "type": Sequelize.ARRAY(Sequelize.STRING),
                "field": "p1choice",
                "allowNull": true
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "user_game_histories",
            "p2username",
            {
                "type": Sequelize.STRING,
                "field": "p2username",
                "allowNull": true
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "user_game_histories",
            "p1username",
            {
                "type": Sequelize.STRING,
                "field": "p1username",
                "allowNull": true
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
