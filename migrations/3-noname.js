'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * removeColumn "userGameUserId" from table "user_game_histories"
 *
 **/

var info = {
    "revision": 3,
    "name": "noname",
    "created": "2022-06-12T15:29:45.178Z",
    "comment": ""
};

var migrationCommands = [{
    fn: "removeColumn",
    params: ["user_game_histories", "userGameUserId"]
}];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
