'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * changeColumn "p2choice" on table "user_game_histories"
 * changeColumn "p1choice" on table "user_game_histories"
 *
 **/

var info = {
    "revision": 8,
    "name": "noname",
    "created": "2022-06-13T03:53:57.544Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "changeColumn",
        params: [
            "user_game_histories",
            "p2choice",
            {
                "type": Sequelize.ARRAY(Sequelize.STRING),
                "field": "p2choice",
                "allowNull": true
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "user_game_histories",
            "p1choice",
            {
                "type": Sequelize.ARRAY(Sequelize.STRING),
                "field": "p1choice",
                "allowNull": true
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
