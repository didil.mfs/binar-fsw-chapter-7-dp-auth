// controllers/apiv1Controller.js
let users = require("../database/user.json");
module.exports = {
  // API V1 Controller
  users: (req, res) => {
    res.status(200).json(users);
  },
  info: (req, res) => {
    const user = users.find((i) => i.id === +req.params.id);
    res.status(200).json(user);
  },
  create: (req, res) => {
    // Destruct req.body
    const { username, password, fullname } = req.body;
    // Get ID
    const id = users[users.length - 1].id + 1;
    const user = {
      id,
      username,
      password,
      fullname,
    };
    // Simpan ke users array
    users.push(user);
    res.status(201).json(user);
  },
  update: (req, res) => {
    let user = users.find((i) => i.id === +req.params.id);
    const params = {
      username: req.body.username,
      password: req.body.password,
      fullname: req.body.fullname,
    };
    user = { ...user, ...params };
    users = users.map((i) => (i.id === user.id ? user : i));
    res.status(200).json(user);
  },
  delete: (req, res) => {
    users = users.filter((i) => i.id !== +req.params.id);
    res.status(200).json({
      message: `User dengan id ${req.params.id} sudah berhasil dihapus`,
    });
  },
};
