// controllers/authController.js

let users = require("../database/user.json");
const { user_game, user_game_biodata, user_game_history } = require("../models");

function format(user) {
  const { user_id, username, role } = user;
  return {
    id: user_id,
    username,
    role,
    accessToken: user.generateToken(),
  };
}

module.exports = {
  login: (req, res) => {
    let request = req.body;
    let usersData = users;
    for (let i = 0; i < usersData.length; i++) {
      const user = usersData[i];
      if (request.username == user.username && request.password == user.password) {
        return res.redirect("/dashboard");
      }
    }
    console.log("cek lagi...");
    user_game
      .authenticate(req.body)
      .then((user) => {
        console.log(user.username, "berhasil login!");
        let user_data = format(user);
        res.header("Authorization", user_data.accessToken);
        res.json({ user_data, pesan: "berhasil login" });
      })
      .catch((err) => res.send(err));
  },
  register: (req, res, next) => {
    user_game
      .register(req.body)
      .then(() => {
        res.redirect("/login");
      })
      .catch((err) => next(err));
  },
  whoami: (req, res) => {
    const currentUser = req.user;
    res.json({
      user_id: currentUser.user_id,
      username: currentUser.username,
    });
  },
};
