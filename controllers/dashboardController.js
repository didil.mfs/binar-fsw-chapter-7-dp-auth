const { user_game, user_game_biodata, user_game_history } = require("../models");
module.exports = {
  dashboardView: (req, res) => {
    user_game.findAll({ include: [user_game_biodata] }).then((users) => {
      res.render("dashboard", { users });
    });
  },
  dashboardRegisterUserLogic: (req, res) => {
    user_game
      .create(
        {
          username: req.body.username,
          password: req.body.password,
          user_game_biodatum: {
            fullname: req.body.fullname,
            email: req.body.email,
            phone: req.body.phone,
          },
        },
        { include: [{ association: user_game.hasOne(user_game_biodata) }] }
      )
      .then((user) => {
        res.status(200);
        res.redirect("/dashboard");
      });
  },
  dashboardDeleteUserLogic: (req, res) => {
    console.log(req.body.username);
    user_game.destroy({ where: { username: req.body.username } }).then((user) => {
      console.log("User ", req.body.username, " deleted");
      res.status(200);
      res.redirect("/dashboard");
    });
  },
  dashboardUpdateBiodataView: (req, res) => {
    user_game.findOne({ where: { username: req.params.username }, include: [user_game_biodata] }).then((user) => {
      res.render("dashboard_update_biodata", { user });
      console.log(user);
      // res.send(user);
    });
  },
  dashboardUpdateBiodataLogic: (req, res) => {
    user_game.findOne({ where: { username: req.params.username } }).then((user) => {
      user_game_biodata
        .update(
          {
            fullname: req.body.fullname,
            email: req.body.email,
            phone: req.body.phone,
          },
          { where: { userGameUserId: user.user_id } }
        )
        .then((user) => {
          console.log(user);
          res.status(200);
          res.redirect("/dashboard");
        });
    });
  },
  dashboardUpdatePasswordView: (req, res) => {
    user_game.findOne({ where: { username: req.params.username } }).then((user) => {
      res.render("dashboard_update_password", { user });
      console.log(user);
      // res.send(user);
    });
  },
  dashboardUpdatePasswordLogic: (req, res) => {
    user_game.findOne({ where: { username: req.params.username } }).then((user) => {
      user_game
        .update(
          {
            password: req.body.password,
          },
          { where: { user_id: user.user_id } }
        )
        .then((user) => {
          console.log(user);
          res.status(200);
          res.redirect("/dashboard");
        });
    });
  },
};
