const { user_game, user_game_biodata, user_game_history } = require("../models");
const { sequelize } = require("../models");
module.exports = {
  vscom: (req, res) => {
    res.render("play");
  },
  history: (req, res) => {
    user_game_history.findAll().then((games) => {
      res.render("history", { games });
    });
  },
  historyJSON: (req, res) => {
    user_game_history.findAll().then((games) => {
      res.json(games);
    });
  },
  createRoom: (req, res, next) => {
    user_game_history
      .createroom(req.body)
      .then((room) => {
        res.json(room.id);
      })
      .catch((err) => next(err));
  },
  fight: async (req, res) => {
    const { username, choice } = req.body;
    let msg = "pemain " + username + " memilih " + choice;
    await user_game_history.findOne({ where: { id: req.params.id } }).then(async (room) => {
      //   console.log(room);
      if (room.p1username == null && room.p2username != username) {
        await user_game_history.update({ p1username: username, p1choice: [choice] }, { where: { id: room.id } });
        // return res.send(msg);
      } else if (room.p1username != username && room.p2username == null) {
        await user_game_history.update({ p2username: username, p2choice: [choice] }, { where: { id: room.id } });
        // return res.send(msg);
      } else {
        if (room.p1username == username && room.p1choice.length < 3) {
          await user_game_history.update({ p1choice: sequelize.fn("array_append", sequelize.col("p1choice"), choice) }, { where: { id: room.id } });
        } else if (room.p2username == username && room.p2choice.length < 3) {
          await user_game_history.update({ p2choice: sequelize.fn("array_append", sequelize.col("p2choice"), choice) }, { where: { id: room.id } });
        } else {
          //   res.send("Permainan hanya 3 ronde, anda sudah bermain tiga kali");
          return res.send("Permainan hanya 3 ronde, anda sudah bermain tiga kali");
        }
      }
      console.log(room.p1choice);
      console.log(room.p2choice);
      if (room.p1choice == null && room.p2choice == null) {
        res.send("Permainan round 1 dimulai. Pemain sudah memilih, waiting for opponent response");
      } else if (room.p1choice.length == 1 && room.p2choice != null) {
        console.log("p1" + room.p1choice);
        console.log("p2" + room.p2choice);
        res.send("permainan round 2 dimulai. Pemain sudah memilih, waiting for opponent response");
      } else if (room.p1choice.length == 2 && room.p2choice.length == 2) {
        console.log(room.p1choice);
        console.log(room.p2choice);
        res.send("permainan round 3 dimulai. Pemain sudah memilih, waiting for opponent response");
      }
    }),
      await user_game_history.findOne({ where: { id: req.params.id } }).then((room) => {
        // console.log(room);
        let win = null;
        let lose = null;
        let p1score = room.p1score;
        let p2score = room.p2score;
        if (room.p1choice != null && room.p2choice != null) {
          if (room.p1choice.length == 1 && room.p2choice.length == 1) {
            let i = 0;
            if ((room.p1choice[i] == "R" && room.p2choice[i] == "R") || (room.p1choice[i] == "P" && room.p2choice[i] == "P") || (room.p1choice[i] == "S" && room.p2choice[i] == "S")) {
              p1score += 0;
              p2score += 0;
            } else if ((room.p1choice[i] == "R" && room.p2choice[i] == "S") || (room.p1choice[i] == "P" && room.p2choice[i] == "R") || (room.p1choice[i] == "S" && room.p2choice[i] == "P")) {
              p1score += 1;
              p2score += 0;
            } else if ((room.p1choice[i] == "R" && room.p2choice[i] == "P") || (room.p1choice[i] == "P" && room.p2choice[i] == "S") || (room.p1choice[i] == "S" && room.p2choice[i] == "R")) {
              p2score += 1;
              p1score += 0;
            }
            if (p1score > p2score) {
              win = room.p1username;
              lose = room.p2username;
            } else if (p1score < p2score) {
              win = room.p2username;
              lose = room.p1username;
            } else {
              win = "draw";
              lose = "draw";
            }
            user_game_history.update({ win: win, lose: lose, p1score: p1score, p2score: p2score }, { where: { id: room.id } });
            return res.send("Pemenangnya adalah " + win + ". Score P1: " + p1score + " , Score P2: " + p2score);
          }
          if (room.p1choice.length == 2 && room.p2choice.length == 2) {
            let i = 1;
            if ((room.p1choice[i] == "R" && room.p2choice[i] == "R") || (room.p1choice[i] == "P" && room.p2choice[i] == "P") || (room.p1choice[i] == "S" && room.p2choice[i] == "S")) {
              p1score += 0;
              p2score += 0;
            } else if ((room.p1choice[i] == "R" && room.p2choice[i] == "S") || (room.p1choice[i] == "P" && room.p2choice[i] == "R") || (room.p1choice[i] == "S" && room.p2choice[i] == "P")) {
              p1score += 1;
              p2score += 0;
            } else if ((room.p1choice[i] == "R" && room.p2choice[i] == "P") || (room.p1choice[i] == "P" && room.p2choice[i] == "S") || (room.p1choice[i] == "S" && room.p2choice[i] == "R")) {
              p2score += 1;
              p1score += 0;
            }
            if (p1score > p2score) {
              win = room.p1username;
              lose = room.p2username;
            } else if (p1score < p2score) {
              win = room.p2username;
              lose = room.p1username;
            } else {
              win = "draw";
              lose = "draw";
            }
            user_game_history.update({ win: win, lose: lose, p1score: p1score, p2score: p2score }, { where: { id: room.id } });
            return res.send("Pemenangnya adalah " + win + ". Score P1: " + p1score + " , Score P2: " + p2score);
          }
          if (room.p1choice.length == 3 && room.p2choice.length == 3) {
            let i = 2;
            if ((room.p1choice[i] == "R" && room.p2choice[i] == "R") || (room.p1choice[i] == "P" && room.p2choice[i] == "P") || (room.p1choice[i] == "S" && room.p2choice[i] == "S")) {
              p1score += 0;
              p2score += 0;
            } else if ((room.p1choice[i] == "R" && room.p2choice[i] == "S") || (room.p1choice[i] == "P" && room.p2choice[i] == "R") || (room.p1choice[i] == "S" && room.p2choice[i] == "P")) {
              p1score += 1;
              p2score += 0;
            } else if ((room.p1choice[i] == "R" && room.p2choice[i] == "P") || (room.p1choice[i] == "P" && room.p2choice[i] == "S") || (room.p1choice[i] == "S" && room.p2choice[i] == "R")) {
              p2score += 1;
              p1score += 0;
            }
            if (p1score > p2score) {
              win = room.p1username;
              lose = room.p2username;
            } else if (p1score < p2score) {
              win = room.p2username;
              lose = room.p1username;
            } else {
              win = "draw";
              lose = "draw";
            }
            user_game_history.update({ win: win, lose: lose, p1score: p1score, p2score: p2score }, { where: { id: room.id } });
            return res.send("Pemenangnya adalah " + win + ". Score P1: " + p1score + " , Score P2: " + p2score);
          }
        }
        console.log(p1score);
        console.log(p2score);
      });
  },
};
