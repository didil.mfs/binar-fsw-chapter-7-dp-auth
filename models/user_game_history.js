"use strict";
const { Model } = require("sequelize");
const user_game = require("./user_game");
module.exports = (sequelize, DataTypes) => {
  class user_game_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // // define association here
      // this.belongsTo(models.user_game, {
      //   onDelete: "CASCADE",
      // });
    }
    static createroom = ({ roomname }) => {
      return this.create({
        roomname: roomname,
      });
    };
    // static fight = ({ id, username, choice }) => {

    //   return this.update({
    //     roomname: roomname,
    //   });
    // };
  }
  user_game_history.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      roomname: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      p1username: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      p2username: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      p1choice: {
        type: DataTypes.ARRAY(DataTypes.STRING),
        allowNull: true,
      },
      p2choice: {
        type: DataTypes.ARRAY(DataTypes.STRING),
        allowNull: true,
      },
      p1score: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      p2score: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      win: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      lose: {
        type: DataTypes.STRING,
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: "user_game_history",
    }
  );
  return user_game_history;
};
