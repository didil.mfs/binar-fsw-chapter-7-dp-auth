"use strict";
const { Model } = require("sequelize");
// const user_game_biodata = require("./user_game_biodata");
// const user_game_history = require("./user_game_history");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// const user_game_biodata = sequelize.models.user_game_biodata;

// const user_game_biodata = sequelize.import("./user_game_biodata.js");

module.exports = (sequelize, DataTypes) => {
  class user_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasOne(models.user_game_biodata, {
        onDelete: "CASCADE",
      });
    }

    static #encrypt = (password) => bcrypt.hashSync(password, 10);

    static register = ({ username, password, fullname, email, phone }) => {
      const encryptedPassword = this.#encrypt(password);
      console.log("user_game_biodata", sequelize);

      const user_game_biodata = sequelize.models.user_game_biodata;
      const user_game_history = sequelize.models.user_game_history;

      return this.create(
        {
          username: username,
          password: encryptedPassword,
          user_game_biodatum: {
            fullname: fullname,
            email: email,
            phone: phone,
          },
        },
        { include: [{ association: this.hasOne(user_game_biodata) }] }
      );
    };

    checkPassword = (password) => bcrypt.compareSync(password, this.password);
    /* Method ini kita pakai untuk membuat JWT */
    generateToken = () => {
      // Jangan memasukkan password ke dalam payload
      const payload = {
        user_id: this.user_id,
        username: this.username,
      };
      // Rahasia ini nantinya kita pakai untuk memverifikasi apakah token ini benar-benar berasal dari aplikasi kita
      const rahasia = "rahasia";
      // Membuat token dari data-data diatas
      const token = jwt.sign(payload, rahasia);
      return token;
    };

    static authenticate = async ({ username, password }) => {
      try {
        const user = await this.findOne({ where: { username } });
        if (!user) return Promise.reject("User tidak ada, daftar dulu!");
        const isPasswordValid = user.checkPassword(password);
        if (!isPasswordValid) return Promise.reject("Password salah, cek lagi!");
        return Promise.resolve(user);
      } catch (err) {
        return Promise.reject(err);
      }
    };
  }

  user_game.init(
    {
      user_id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      username: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      role: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "user",
      },
    },
    {
      sequelize,
      modelName: "user_game",
    }
  );
  return user_game;
};
