const express = require("express");
const router = express.Router();

router.use(function timeLog(req, res, next) {
  console.log("Time: ", Date.now());
  next();
});

const restrict = require("./middlewares/restrict");

// MCR & API
const apiv1 = require("./router/apiv1.js");
const game = require("./router/gameapi.js");
router.use("/api/v1", apiv1);
router.use("/play", game);

// MVC
const homeController = require("./controllers/homeController");
const aboutController = require("./controllers/aboutController");
const authController = require("./controllers/authController");
const dashboardController = require("./controllers/dashboardController");

// HOME
router.get("/", homeController.home);
router.get("/about", aboutController.about);

// AUTH
router.get("/login", (req, res) => res.render("login"));
router.post("/login", authController.login);
router.get("/register", (req, res) => res.render("register"));
router.post("/register", authController.register);
router.get("/whoami", restrict, authController.whoami);

// Dashboard
router.get("/dashboard", dashboardController.dashboardView);
router.post("/dashboard", dashboardController.dashboardRegisterUserLogic);
router.delete("/dashboard", dashboardController.dashboardDeleteUserLogic);
router.get("/dashboard/update/biodata/:username", dashboardController.dashboardUpdateBiodataView);
router.post("/dashboard/update/biodata/:username", dashboardController.dashboardUpdateBiodataLogic);
router.get("/dashboard/update/password/:username", dashboardController.dashboardUpdatePasswordView);
router.post("/dashboard/update/password/:username", dashboardController.dashboardUpdatePasswordLogic);

module.exports = router;
