"use strict";

const bcrypt = require("bcrypt");
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    let users = [
      {
        username: "masarie",
        password: bcrypt.hashSync("masarie", 10),
        role: "admin",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        username: "masbezos",
        password: bcrypt.hashSync("masbezos", 10),
        role: "admin",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ];
    await queryInterface.bulkInsert("user_games", users);

    const userIDs = await queryInterface.sequelize.query(`SELECT user_id from user_games;`);
    const userIDRows = userIDs[0];

    let user_biodata = [
      {
        fullname: "Mas Arie",
        phone: 6243214321,
        email: "masarie@binar.com",
        createdAt: new Date(),
        updatedAt: new Date(),
        userGameUserId: userIDRows[0].user_id,
      },
      {
        fullname: "Jeff Bezos",
        phone: 6212341234,
        email: "jeffbezos@amazon.com",
        createdAt: new Date(),
        updatedAt: new Date(),
        userGameUserId: userIDRows[1].user_id,
      },
    ];
    await queryInterface.bulkInsert("user_game_biodata", user_biodata);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("user_games", null, {});
    await queryInterface.bulkDelete("user_game_biodata", null, {});
  },
};
