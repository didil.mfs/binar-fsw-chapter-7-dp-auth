const router = require("express").Router();
const apiv1Controller = require("../controllers/apiv1Controller");

router.get("/users", apiv1Controller.users);
router.get("/users/:id", apiv1Controller.info);
router.post("/users/", apiv1Controller.create);
router.put("/users/:id", apiv1Controller.update);
router.delete("/users/:id", apiv1Controller.delete);

module.exports = router;
