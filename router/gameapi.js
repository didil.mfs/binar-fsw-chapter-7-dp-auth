const router = require("express").Router();
const gameController = require("../controllers/gameController");

const restrict = require("../middlewares/restrict");

router.get("/", gameController.vscom);
router.post("/create-room", restrict, gameController.createRoom);
router.post("/fight/:id", restrict, gameController.fight);
router.get("/history", restrict, gameController.history);
router.get("/history/json", restrict, gameController.historyJSON);

module.exports = router;
